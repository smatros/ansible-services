Stardst services
===
Deploy services on single virtual machine

How to install:
---
 1. Install VirtualBox
 2. Install Vagrant
 3. Install Ansible

Deployment flow:
---
 1. Generate ssh-keys for GitLab
 2. Add public key into User profile on GitLab
 3. Copy and paste public and private keys (id_rsa and id_rsa.pub) into **ansible/roles/common/files**
 4. Install VirtualBox
 5. Install Vagrant
 6. Install Ansible 
 7. Run vagrant up

Virtual Machine contains:
---
 - Ubuntu (git, mc, supervisor)
 - nginx
 - PHP 7.0.* (mysql, fpm, curl, mbstring, mcrypt, xdebug, zip, xml, bcmath, gettext)
 - MariaDB
 - RabbitMQ 3.6.2-1
 - PHPUnit
 - Composer
 - xDebug

Add new service (each service has separate role)
---
 1. Create directory service_name in **ansible/roles** (you can copy and paste from example service)
 2. Fill **ansible/roles/service_name/vars/main.yml** with correct values
 3. And a new role into **playbook.yml** with service_name

***